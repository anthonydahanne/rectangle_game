from tkinter import*
import tkinter
import random
global target
global cursor
global root
global bouton
global score_label

class Chrono(tkinter.Label):
    def __init__(self, root, game_duration):
        tkinter.Label.__init__(self,root,text='Starting...')

        self.time_remaining= game_duration
        self.font=('Helvetica', 18, 'normal')
        self.__setitem__('font',self.font)
        self.after(1000,self.count)

    def update_label(self):
        if int(self.time_remaining)==0:
            return str('mission over')
        else:
            return str(self.time_remaining) + 'secondes'

    def count(self):
        global target_hit
        self.time_remaining -= 1
        if target_hit==1:
            self.time_remaining += 1
            target_hit=0
        self.__setitem__('text', self.update_label())
        if self.time_remaining==0:
            global save_button
            print('Mission over')
            save_button=Button(root, text='Save score and name?', command=save_score_and_name)
            save_button.pack()
            return save_button
        self.after(1000,self.count)

def update_score_label():
    global current_score
    global score_label
    current_score += 1
    current_score_string='vous avez', current_score, 'points'
    score_label=Label(root, text=current_score_string)
    score_label.place(x=10, y=210)

def move_target():
    global target_first_boundary_x
    global target_second_boundary_x
    global target_first_boundary_y
    global target_second_boundary_y
    global target
    # change target position, with a random one
    target_first_boundary_x=random.randint(0, 190)
    target_second_boundary_x= target_first_boundary_x + 10
    target_first_boundary_y=random.randint(0, 190)
    target_second_boundary_y= target_first_boundary_y + 10
    target =main_canvas.create_rectangle(target_first_boundary_x, target_first_boundary_y, target_second_boundary_x, target_second_boundary_y)

def check_target_hit():
    global target_hit
    if target_first_boundary_y-1<cursor_first_boundary_y<target_second_boundary_y+1:
        if target_first_boundary_x-1<cursor_first_boundary_x<target_second_boundary_x+1:
            main_canvas.delete(target)
            move_target()
            update_score_label()
            target_hit=1

def save_score_and_name():
    user_name=raw_input('what is ur name?')
    data_file=open(DATA_TXT, mode='w')
    data_file.write(str(current_score))
    data_file.close()
    name_file=open(NAME_TXT, mode='w')
    name_file.write(user_name)
    name_file.close()

def on_key_down(event):
    global cursor
    global cursor_second_boundary_x
    global cursor_first_boundary_x
    global cursor_first_boundary_y
    global cursor_second_boundary_y
    key=event.keysym
    if key == 'Up':
        cursor_first_boundary_y= cursor_first_boundary_y - 10
        cursor_second_boundary_y= cursor_second_boundary_y - 10
    elif key == 'Down':
        cursor_first_boundary_y= cursor_first_boundary_y + 10
        cursor_second_boundary_y= cursor_second_boundary_y + 10
    elif key == 'Right':
        cursor_first_boundary_x= cursor_first_boundary_x + 10
        cursor_second_boundary_x= cursor_second_boundary_x + 10
    elif key == 'Left':
        cursor_first_boundary_x= cursor_first_boundary_x - 10
        cursor_second_boundary_x= cursor_second_boundary_x - 10

    main_canvas.delete(cursor)
    cursor=main_canvas.create_rectangle(cursor_first_boundary_x, cursor_first_boundary_y, cursor_second_boundary_x, cursor_second_boundary_y, fill="blue")
    check_target_hit()

def print_scores():
    data_file=open(DATA_TXT, mode='r')
    print (data_file.read())
    data_file.close()
    name_file=open(NAME_TXT, mode='r')
    print(name_file.read())
    name_file.close()

def start_new_game():
    global chrono
    global target_hit
    global current_score
    global cursor
    global cursor_second_boundary_x
    global cursor_first_boundary_x
    global cursor_first_boundary_y
    global cursor_second_boundary_y

    # in case it's not the first game, let's delete old stuff
    if 'chrono' in globals():
        chrono.destroy()
    if 'save_button' in globals():
        save_button.destroy()
    if 'cursor' in globals():
        main_canvas.delete(cursor)
    if 'target' in globals():
        main_canvas.delete(target)

    # re init everything
    target_hit=0
    current_score=0
    cursor_first_boundary_x=0
    cursor_first_boundary_y=0
    cursor_second_boundary_x=3
    cursor_second_boundary_y=3
    cursor = main_canvas.create_rectangle(cursor_first_boundary_x, cursor_first_boundary_y, cursor_second_boundary_x, cursor_second_boundary_y, fill="blue")

    chrono = Chrono(root,13)
    chrono.pack()
    move_target()

def init_game():
    bouton=Button(root, text="COMMENCER LE BO JEU", command=start_new_game)
    bouton.pack()
    score=Button(root, text="REGARDER LES SCORES", command=print_scores)
    score.pack()
    main_canvas.focus_set()
    main_canvas.bind("<Key>", on_key_down)
    main_canvas.pack()
    root.mainloop()


NAME_TXT = 'name.txt'
DATA_TXT = 'data.txt'
# try to access or create the 2 files if they don't already exist
name_file=open(NAME_TXT, mode='a')
name_file.close()
data_file=open(DATA_TXT, mode='a')
data_file.close()

root = Tk()
main_canvas=Canvas(root, width=200, height=200, bg='white')

# initialize the game
init_game()
